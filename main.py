from dataset_loader import *
from utils import *
from helpers import *

import matplotlib.pyplot as plt
import numpy as np

train_x_orig, train_y_orig, test_x_orig, test_y_orig = load_dataset()

img_size = train_x_orig.shape[1], train_x_orig.shape[2], train_x_orig.shape[3]

m_train = train_x_orig.shape[0]
m_test = test_x_orig.shape[0]

num_px = train_x_orig.shape[1]

# Transform data in desirable vectorized form
train_x = train_x_orig.reshape(m_train, -1).T / 255
test_x = test_x_orig.reshape(m_test, -1).T / 255

# Converting array to row vector
train_y = np.array([train_y_orig])
test_y = np.array([test_y_orig])

# Layers dimensions
n_x = train_x.shape[0]
n_y = train_y.shape[0]

# Training the model with two layers
# params = two_layer_model(train_x, train_y, (n_x, 7, n_y), num_iterations=2500)
# predict(train_x, train_y, params)

# Layer dimensions for N layer NN
layers_dims = [n_x, 20, 7, 5, n_y]

params, costs = n_layer_model(train_x, train_y, layers_dims, num_iterations=2500)

# Plot the cost as function of number of iterations
plt.plot(np.squeeze(costs))
plt.ylabel('Cost')
plt.xlabel('Iterations (in hundreds)')
plt.show()

# Getting predictions with computed parameters
train_predicts = predict(train_x, train_y, params)
test_predicts = predict(test_x, test_y, params)

# Loop through several images in train set (20)
show_images(train_x_orig[:20], train_predicts[:, :20], 'cat')

print('\nNow on the test set.. ')
# Loop through several images in test set
show_images(test_x_orig[:20], test_predicts[:, :20], 'cat')
