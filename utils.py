import numpy as np


# Activation functions used in NN
def sigmoid(z):
    cache = z
    return 1 / (1 + np.exp(-z)), cache


def relu(z):
    cache = z
    return np.maximum(0, z), cache


def sigmoid_backward(da, cache):
    """
    :param da: post-activation gradient
    :param cache: z
    :return: gradient with respect to z (dz)
    """

    z = cache

    # dz(l) = da(l + 1)  * g'(z(i))
    # sigmoid'(z) = sigmoid(z) * (1 - sigmoid(z))

    s, _ = sigmoid(z)
    dz = da * s * (1 - s)

    return dz


def relu_backward(da, cache):
    """
    :param da: post activation gradient
    :param cache:
    :return:
    """
    z = cache

    dz = np.array(da, copy=True)

    # For z <= 0, dz is 0 as well
    dz[z <= 0] = 0

    return dz


# Initialize parameters for NN with one hidden layer
def init_params(n_x, n_h, n_y):
    """
    :param n_x: size of input layer or number of features per x
    :param n_h: size of middle / hidden layer
    :param n_y: size of output layer
    :return: params (w, b) for hidden and output layer
    """
    w1 = np.random.randn(n_h, n_x) * 0.01
    b1 = np.zeros((n_h, 1))
    w2 = np.random.randn(n_y, n_h) * 0.01
    b2 = np.zeros((n_y, 1))

    return {
        'w1': w1,
        'b1': b1,
        'w2': w2,
        'b2': b2
    }


def init_params_deep(dims):
    """
    :param dims: list containing sizes for each layer
    :return: params (w, b) for each layer with randomized/zero values
    """
    params = {}
    n_l = len(dims)  # number of layers in the network

    for i in range(1, n_l):
        params['w%d' % i] = np.random.randn(dims[i], dims[i - 1]) / np.sqrt(dims[i - 1])
        params['b%d' % i] = np.zeros((dims[i], 1))

    return params


# Calculating single linear (part of) activation (Z)
def linear_forward(a, w, b):
    """
    :param a: Activations of previous layer
    :param w: Weights of current layer
    :param b: Bias of current layer
    :return: z, cache for current layer
    """

    z = w.dot(a) + b
    cache = (a, w, b)

    return z, cache


def linear_activation_forward(a_prev, w, b, activation):
    """
    :param a_prev: Activations of previous layer
    :param w: Weights of current layer
    :param b: Bias of current layer
    :param activation: Activation function
    :return: a, cache for current layer
        activation_cache contains z for specific layer,
        linear_cache contains a, w, b for specific layer
    """
    z, linear_cache = linear_forward(a_prev, w, b)

    if activation == 'sigmoid':
        a, activation_cache = sigmoid(z)
    elif activation == 'relu':
        a, activation_cache = relu(z)

    return a, (linear_cache, activation_cache)


def forward_prop(x, params):
    """
    :param x: X vector containing our data
    :param params: Weights for each layer
    :return: Activations for output layer and cache (linear & activation cache)
    """
    caches = []
    a = x
    n_layers = len(params) // 2

    # ReLU for n_layers - 1
    for i in range(1, n_layers):
        a_prev = a
        a, cache = linear_activation_forward(a_prev, params['w%d' % i], params['b%d' % i], 'relu')
        caches.append(cache)

    # Sigmoid for the output layer
    al, cache = linear_activation_forward(a, params['w%d' % n_layers], params['b%d' % n_layers], 'sigmoid')

    caches.append(cache)

    return al, caches


def compute_cost(al, y):
    """
    :param al: Activations for output layer
    :param y: Vector with y values
    :return: cost: Computed cost
    """
    m = y.shape[1]

    # cost = (-1 / m) * np.sum(y * np.log(al) + (1 - y) * np.log(1 - al))
    cost = (1. / m) * (-np.dot(y, np.log(al).T) - np.dot(1-y, np.log(1 - al).T))

    return np.squeeze(cost)


def linear_backward(dz, cache):
    """
    :param dz: previous linear gradient
    :param cache: (a_prev, w, b)
    :return: da_prev, dw, db
    """

    a_prev, w, b = cache
    m = a_prev.shape[1]

    dw = np.dot(dz, a_prev.T) / m
    db = np.sum(dz, axis=1, keepdims=True) / m
    da_prev = np.dot(w.T, dz)

    return da_prev, dw, db


def linear_activation_backward(da, cache, activation):
    """
    :param da: post-activation for current layer
    :param cache: activation_cache, linear_cache
    :param activation: activation method for layer
    :return: da_prev, w, b
    """
    linear_cache, activation_cache = cache

    if activation == 'relu':
        dz = relu_backward(da, activation_cache)
    elif activation == 'sigmoid':
        dz = sigmoid_backward(da, activation_cache)

    # For the next (backwards) layer
    da_prev, dw, db = linear_backward(dz, linear_cache)

    return da_prev, dw, db


def model_backward(al, y, caches):
    """
    :param al: Activation of output layer (from forward propagation)
    :param y: Y values vector
    :param caches: tuple with linear cache and activation cache:
        Caches for each layer (from forward propagation)
    :return: grads: Gradients of params in respect to loss
    """
    grads = {}
    m = al.shape[1]
    n_l = len(caches)  # Number of layers

    # Calculating backward activation for output layer
    dal = -(np.divide(y, al) - np.divide(1 - y, 1 - al))

    # Grads for output layer (sigmoid backward)
    current_cache = caches[n_l - 1]
    grads['da%d' % (n_l - 1)], grads['dw%d' % n_l], grads['db%d' % n_l] = \
        linear_activation_backward(dal, current_cache, 'sigmoid')

    # Calculating gradients for the rest
    for i in reversed(range(n_l - 1)):
        current_cache = caches[i]

        grads['da%d' % i], grads['dw%d' % (i + 1)], grads['db%d' % (i + 1)] = \
            linear_activation_backward(grads['da%d' % (i + 1)], current_cache, 'relu')

    return grads


def update_params(params, grads, learning_rate):
    """
    :param params: Weights to be updateds
    :param grads: Gradients computed with back propagation
    :param learning_rate: _
    :return: params: Update params
    """
    n_layers = len(params) // 2

    for i in range(n_layers):
        params['w%d' % (i + 1)] -= learning_rate * grads['dw%d' % (i + 1)]
        params['b%d' % (i + 1)] -= learning_rate * grads['db%d' % (i + 1)]

    return params


def two_layer_model(x, y, layers_dims, learning_rate=0.0075, num_iterations=3000):
    """
    :param x: X vector
    :param y: Y vector
    :param layers_dims: A tuple with three sizes
    :param learning_rate: Learning rate for params
    :param num_iterations: Number of iterations
    :return: Params for first and output layer
    """
    grads = {}
    costs = []
    (n_x, n_h, n_y) = layers_dims

    params = init_params(n_x, n_h, n_y)

    w1 = params['w1']
    b1 = params['b1']
    w2 = params['w2']
    b2 = params['b2']

    for i in range(0, num_iterations):
        # Forward propagation
        a1, cache1 = linear_activation_forward(x, w1, b1, 'relu')
        a2, cache2 = linear_activation_forward(a1, w2, b2, 'sigmoid')

        # Compute cost for current iteration
        cost = compute_cost(a2, y)
        costs.append(cost)

        if i % 100 == 0:
            print('i={}, cost={}'.format(i, cost))

        # Computing grads (the one for the output layer is specific)
        da2 = -(np.divide(y, a2) - np.divide(1 - y, 1 - a2))

        # Backward propagation
        da1, dw2, db2 = linear_activation_backward(da2, cache2, 'sigmoid')
        da0, dw1, db1 = linear_activation_backward(da1, cache1, 'relu')

        grads['dw1'] = dw1
        grads['db1'] = db1
        grads['dw2'] = dw2
        grads['db2'] = db2

        # Updating params using computed gradients
        params = update_params(params, grads, learning_rate)

        # Unpack and assign it for the next iteration
        w1 = params['w1']
        b1 = params['b1']
        w2 = params['w2']
        b2 = params['b2']

    # Return computed params
    return params


def n_layer_model(x, y, layers_dims, learning_rate=0.0075, num_iterations=3000):
    """
    :param x: X matrix
    :param y: Y vector
    :param layers_dims: A list of layer sizes
    :param learning_rate: Learning rate for params
    :param num_iterations: Number of iterations
    :return: Params for each layer
    """
    params = init_params_deep(layers_dims)
    costs = []

    for i in range(0, num_iterations):
        # Forward propagation
        al, caches = forward_prop(x, params)

        # Compute cost
        cost = compute_cost(al, y)

        # Printing cost after 100 iterations
        if i % 100 == 0:
            print('i={}, cost={}'.format(i, cost))
            costs.append(cost)

        # Backward propagation and computing gradients
        grads = model_backward(al, y, caches)

        # Updating parameters
        params = update_params(params, grads, learning_rate)

    return params, costs


def predict(x, y, params):
    """
    :param x: X matrix
    :param y: Y vector
    :param params: Computed params
    :return: Predictions vector
    """
    m = x.shape[1]
    predictions = np.zeros(y.shape)

    # Forward propagation
    probabilities, caches = forward_prop(x, params)

    # Convert probabilities to 0/1 predictions
    for i in range(0, probabilities.shape[1]):
        if probabilities[0, i] > 0.5:
            predictions[0, i] = 1
        else:
            predictions[0, i] = 0

    print("Accuracy: " + str(np.sum((predictions == y) / m)))

    return predictions
