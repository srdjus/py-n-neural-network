import h5py
import numpy as np


def load_dataset():
    with h5py.File('./datasets/train.h5', 'r') as f_train:
        train_x = np.array(f_train['train_set_x'])
        train_y = np.array(f_train['train_set_y'])

    with h5py.File('./datasets/test.h5', 'r') as f_test:
        test_x = np.array(f_test['test_set_x'])
        test_y = np.array(f_test['test_set_y'])

    return train_x, train_y, test_x, test_y
