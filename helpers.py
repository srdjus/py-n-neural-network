import matplotlib.pyplot as plt


def show_images(x, predictions, label):

    for i in range(0, predictions.shape[1]):
        plt.imshow(x[i])
        plt.title(label if predictions[0, i] else 'not a {}'.format(label))
        plt.show()
